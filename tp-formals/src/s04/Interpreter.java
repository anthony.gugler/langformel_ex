package s04;

import java.util.HashMap;

public class Interpreter {
    private static Lexer lexer;
    // TODO - A COMPLETER (ex. 3)

    public static int evaluate(String e) throws ExprException {
        lexer = new Lexer(e);
        int res = parseExpr();
        // test if nothing follows the expression...
        if (lexer.crtSymbol().length() > 0)
            throw new ExprException("bad suffix");
        return res;
    }

    private static int parseExpr() throws ExprException {
        int res = 0;
        res += parseTerm();
        while (lexer.isMinus() || lexer.isPlus()) {
            if (lexer.isMinus()) {
                lexer.goToNextSymbol();
                res -= parseTerm();
            } else {
                lexer.goToNextSymbol();
                res += parseTerm();

            }
        }
        return res;
    }

    private static int parseTerm() throws ExprException {
        int res = 0;
        res += parseFact();
        while (lexer.isSlash() || lexer.isStar()) {
            if (lexer.isSlash()) {
                lexer.goToNextSymbol();
                res /= parseTerm();

            } else {
                lexer.goToNextSymbol();
                res *= parseTerm();

            }
        }
        return res;
    }

    private static int parseFact() throws ExprException {
        int res = 0;
        String str = "";
        int arg=0;
        if (lexer.isOpeningParenth()) {
            lexer.goToNextSymbol();
            res += parseExpr();
            if (lexer.isClosingParenth()) {
                lexer.goToNextSymbol();
            } else {
                throw new ExprException("pas -->)");
            }
        } else if (lexer.isIdent()) {
            str += lexer.crtSymbol();
            lexer.goToNextSymbol();

            if (lexer.isOpeningParenth()) {
                lexer.goToNextSymbol();
                arg += parseExpr();
                if (lexer.isClosingParenth()) {
                    lexer.goToNextSymbol();
                } else {
                    throw new ExprException("pas -->)");
                }
            }
            res += applyFct(str, arg);
        } else {
            res += lexer.intFromSymbol();
            lexer.goToNextSymbol();
        }

        return res;
    }

    private static int applyFct(String fctName, int arg) throws ExprException {
        switch (fctName) {
            case "abs":
                return Math.abs(arg);
            case "sqr":
                return arg * arg;
            case "cube":
                return arg * arg * arg;
            case "sqrt":
                return (int) Math.round(Math.sqrt(arg));
            default:
                throw new ExprException("invalid expression");
        }
    }
}
