package s04;

import java.util.ArrayList;

public class InterpreterTest {
  //============================================================
  static class TestData{
    final String expr;
    final int    val;  // expression correct value
    TestData(int v, String e) {
      expr=e; val=v;
    }
  }
  //============================================================
  static ArrayList<TestData> corpusWithoutVars = new ArrayList<TestData>();
  static ArrayList<TestData> corpusWithVars    = new ArrayList<TestData>();

  static {
    corpusWithoutVars.add(new TestData(1, "1" ));
    corpusWithoutVars.add(new TestData(2, "(2)" ));
    corpusWithoutVars.add(new TestData(24, "3+21" ));
    corpusWithoutVars.add(new TestData(7, "2*2+3"  ));
    corpusWithoutVars.add(new TestData(7, " 2  * 2+ 3"  ));
    corpusWithoutVars.add(new TestData(7, "2  *2+ 3 "  ));
    corpusWithoutVars.add(new TestData(7, "3 +2*2"  ));
    corpusWithoutVars.add(new TestData(4, "abs(0-sqr(2))"));
    corpusWithoutVars.add(new TestData(4, "abs(0+sqr(2))"));
    corpusWithoutVars.add(new TestData(2, "(sqrt (4))" ));
    corpusWithoutVars.add(new TestData(12, "2+2+2+2+2+2"));
    corpusWithoutVars.add(new TestData(2, "((abs(sqr(2)-cube(2)))-(sqrt(4)))"));
    corpusWithoutVars.add(new TestData(14, "2*(4+3)"));
    corpusWithoutVars.add(new TestData(-18, "3*(4-sqr(4))/2"));
    
    corpusWithVars.add(new TestData(0, "(4-1)abc -abc"));
    corpusWithVars.add(new TestData(8, "(2)v +v +v +v"));
    corpusWithVars.add(new TestData(3, "(3+ (4-1)v) -v"));
    corpusWithVars.add(new TestData(29, "3*(4-1)abc + (2)*abc + (4+abc)xy + xy"));

  }
  
  static String[] wrongExpressions = {
      " ",
      "2+",
      "3+*2",
      "3a*2",
      "((1+234)*5))",
      "(((1+234)*5)",
      "abso(0-5)",
      "a bs(0-2)",
      "2 2+3",
      "sqrt(0-4)",
      "3-aaa",
      "(3+1)aaa -aab"
  };
  
  public static void generateException(String msg) {
    try { Thread.sleep(300); }
    catch (InterruptedException ex) { }
    throw new RuntimeException(msg);
  }

  public static int testExpr(String expr) throws ExprException {
    System.out.print("'"+ expr + "'");
    int res = Interpreter.evaluate(expr);
    System.out.println("  gives result : " + res);
    return res;
  }

  public static void testCorrectExpr() {
    for (TestData td:corpusWithoutVars) {
      try {
        int res = testExpr(td.expr);
        if (res != td.val) {
          generateException("'" + td.expr + "' returns " + res
                            + " but the correct result is: " + td.val);
        }
      }
      catch (ExprException e) {
        e.printStackTrace();
        generateException("'" + td.expr + "' is wrong due to " + e);
      }
    }
  }

  public static void testExprWithVars() {
    System.out.println();
    for (TestData td:corpusWithVars) {
      try {
        int res = testExpr(td.expr);
        if (res != td.val) {
          generateException("'" + td.expr + "' returns " + res
                            + " but the correct result is: " + td.val);
        }
      } catch (ExprException e) {
        e.printStackTrace();
        generateException("'" + td.expr + "' is wrong due to " + e);
      }
    }
  }

  public static void testWrongExpr() {
    String expr;
    boolean exprIsAccepted;

    System.out.println();
    for (int i = 0; i < wrongExpressions.length; i++) {
      exprIsAccepted = true;
      expr = wrongExpressions[i];
      try {
        testExpr(expr);
      } catch (Exception e) {
        System.out.println(" rejected due to " + e);
        exprIsAccepted = false;
      }
      if (exprIsAccepted) {
        generateException("'" + expr +
                          "' is accepted, but it should be refused ");
      }
    }
  }
  //------------------------------------------------------------
  public static void main(String[] args) {
    testCorrectExpr();
    testWrongExpr();
    testExprWithVars();
    System.out.println("\nTest passed successfully !");
  }
}
